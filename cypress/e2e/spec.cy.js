describe('Front Search for a train trip and succeed', () => {
  it('passes', () => {
    cy.visit('http://localhost:8080/')
    cy.get("#departure").type("Paris");
    cy.get("#arrival").type("Bruxelles");
    cy.get("#date").type("2024-02-06");
    cy.get("#search").click();
    cy.get(".trip").first().should('exist');
  })
})

describe('Front Search for a train and did not succeed', () => {
  it('passes', () => {
    cy.visit('localhost:8080')
    cy.get("#departure").type("Paris");
    cy.get("#arrival").type("Albuquerque");
    cy.get("#date").type("2024-02-06");
    cy.get("#search").click();
    cy.contains("#results", "No trip found.").should('exist');
  })
})

describe('Book a train and delete from basket', () => {
  it('passes', () => {
    cy.visit('localhost:8080')
    cy.get("#departure").type("Paris");
    cy.get("#arrival").type("Bruxelles");
    cy.get("#date").type("2024-02-06");
    cy.get("#search").click();
    cy.get(".book").first().click();
    cy.contains(".selected-trip", "Paris > Bruxelles ");
    //delete all elements
    cy.get(".delete").each(($elem, index, $array) => {
      cy.wrap($elem).click();
    });
    cy.contains("#cart", "No tickets in your cart.")
  })
})


describe('Test buy feature', () => {
  it('passes', () => {
    cy.visit('localhost:8080')
    cy.get("#departure").type("Paris");
    cy.get("#arrival").type("Bruxelles");
    cy.get("#date").type("2024-02-06");
    cy.get("#search").click();
    cy.get(".book").first().click();
    cy.contains(".selected-trip", " Paris > Bruxelles ");
    cy.get("#purchase").click();
    cy.contains("#trips", "Paris > Bruxelles");
  })
})


